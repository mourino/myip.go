package myip

import (
	"fmt"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"
)

type Response struct {
	Ip string
}

const IpapiCo string = "ipapi.co"
const IpifyOrg string = "ipify.org"
const MyexternalipCom string = "myexternalip.com"
const MyipCom string = "myip.com"

func Get(provider string) (string, error) {
	providers := map[string]string {
		IpapiCo: "https://ipapi.co/json/",
		IpifyOrg: "https://api.ipify.org?format=json",
		MyexternalipCom: "https://myexternalip.com/json",
		MyipCom: "https://api.myip.com/",
	}

	url := providers[provider]
	if url == "" {
		return "", errors.New("Invalid provider")
	}

	client := http.Client{
		Timeout: time.Second * 2,
	}

	res, err := client.Get(url)
	if err != nil {
		return "", err
	}
	if res.StatusCode != 200 {
		msg := fmt.Sprintf("%s: %s %s", provider, res.Proto, res.Status)
		return "", errors.New(msg)
	}

	if res.Body == nil {
		return "", errors.New("Empty response from server")
	} else {
		defer res.Body.Close()
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	// fmt.Print(string(body[:]))
	resp := Response{}
	err = json.Unmarshal(body, &resp)
	if err != nil {
		return "", err
	}

	return resp.Ip, nil
}
