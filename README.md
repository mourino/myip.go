# My IP

Get your _public_ IP. This project is both a command line utility and a library.

## Building

To build a binary named `myip` in the current directory just run:

```sh
make
```

Alternatively you can specify a different target:

```sh
make target=$HOME/bin/myip
```

## Running

You can run this utility from the command line:

```sh
myip
```

### Providers

By default it will use `ipify.org` as a provider. You can change this with the
`-p` flag. The currently supported providers are:

* "ipapi.co"
* "ipify.org"
* "myexternalip.com"
* "myip.com"

```sh
myip -p ipapi.co
```

## Library

You can alternatively use it as a library from your own code:

```go
package main

import (
	"fmt"
	myip "gitlab.com/mourino/myip.go"
)

func main() {
	ip, _ := myip.Get(myip.IpifyOrg)
	fmt.Println("Your public IP is:", ip)
}

```

### Providers

To switch providers pass one of the following constants to the `Get()` method:

* `myip.IpapiCo`
* `myip.IpifyOrg`
* `myip.MyexternalipCom`
* `myip.MyipCom`
