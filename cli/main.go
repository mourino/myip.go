package main

import (
	"flag"
	"fmt"
	"myip"
)

func main() {
	var provider string
	defaultProvider := myip.IpifyOrg

	flag.StringVar(&provider, "p", defaultProvider, "Use a different provider (default: " + defaultProvider + ")")
	flag.Parse()

	ip, err := myip.Get(provider)
	if err != nil {
		errorColor := "\033[1;31m"
		colorReset := "\033[0m"
		fmt.Println(errorColor + "[ ERROR ] " + err.Error() + colorReset)
		return
	}

	fmt.Println(ip)
}

var Get = myip.Get
